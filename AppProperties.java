import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.paint.*;
import javafx.scene.shape.*;
import javafx.scene.Group;
import javafx.scene.layout.GridPane;
import javafx.geometry.*;
import javafx.scene.layout.*;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.BorderPane;





public class AppProperties{
	
	private static AppProperties instance = null;
   	
	protected AppProperties() {
    isWinner = true;
  }



   	public static AppProperties getInstance() {
      if(instance == null) {
         instance = new AppProperties();
      }
      return instance;
   }

    public Stage stgContainter;
    public Scene scnMenu, scnGame;
    public FlowPane panel;
    public GridPane panelGame;
    public GridPane panelStatus;
    public BorderPane brdPContainter;

    //Select your difficulty
    public Button btnHard, btnEasy, btnMedium,btnFile;
    public Label lblSelectMode;

    public Label lblSquares;

    public Boolean isWinner;

    public Label lblFile;
    public Button[][] squares;




}