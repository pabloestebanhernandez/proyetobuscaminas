import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.paint.*;
import javafx.scene.shape.*;
import javafx.scene.Group;
import javafx.scene.layout.GridPane;
import javafx.geometry.*;
import javafx.scene.layout.*;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import java.lang.Math;
//<<<<<<< HEAD
import javafx.scene.layout.BorderPane;
//=======
import javafx.scene.Node;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.filechooser.FileNameExtensionFilter;
import javafx.stage.FileChooser.ExtensionFilter;
import java.util.Scanner;
import java.io.FileNotFoundException;


public class HandlerGame{


	private static HandlerGame instance = null;
  	
  	private AppProperties prop;
  	private Boolean[][] bombs ;
  	private int statusSquares  ;
  	private int statusRest;
  	private File fileSquares;

	protected HandlerGame() {
    	prop = AppProperties.getInstance();
  	}

   	public static HandlerGame getInstance() {
      if(instance == null) {
         instance = new HandlerGame();
      }
      return instance;
  	}



	public void selectEasy(ActionEvent e){
	  	int squares = 8;
	  	int bomb = 20;
	    loadGame(squares,bomb);

 	}

  	public void selectMedium(ActionEvent e){
		int squares = 16;
	  	int bomb = 40;
	    loadGame(squares,bomb);
  	}

 	public void selectHard(ActionEvent e)
  	{
  		int squares = 32;
	  	int bomb = 80;
	    loadGame(squares,bomb);
    	prop.stgContainter.setMaximized(true);

  	}

  	public void loadGame(int squares, int bomb){

  		loadBombs(bomb,squares);

  		setStatus(squares,bomb);
	    prop.panelStatus = createPanelStatus();

	    prop.panelGame = createBoard(squares);

	    prop.brdPContainter = new BorderPane();
	    prop.brdPContainter.setCenter(prop.panelGame);
	    prop.brdPContainter.setTop(prop.panelStatus);

	    prop.scnGame = new Scene(prop.brdPContainter, 600,600);
	    prop.stgContainter.setScene(prop.scnGame);
	    prop.stgContainter.show();
  	}

  	private void setStatus(int squares, int bomb){

  		statusSquares = (squares * squares)-bomb;
  		statusRest =0;
  	}

  	private GridPane createPanelStatus(){

  		GridPane grdStatus = new GridPane();
  		String content = "Restantes: " +statusSquares +"\nSeleccionadas :"+statusRest;
  		prop.lblSquares = new Label(content);
  		grdStatus.add(prop.lblSquares,0,0);

  		return grdStatus;

  	}

  	private GridPane createBoard(int squares){

	    GridPane board = new GridPane();
	    prop.squares = new Button[squares][squares];

	    for(int y =0 ;  y < squares ; y++ ){
	      for(int x =0 ;  x < squares ; x++ ){
	          
	        Button temp = new Button ("*");  
	        int[] position = new int[] { x,y };
	        temp.setOnAction(event -> clicked(event,position) );
	        board.add(temp, x, y);
	        prop.squares[x][y] = temp;
	      }
	    }
	    return board;
  	}

  	private void loadBombs(int bomb, int squares){
	  

  		initializatedBombs(squares);

  		int count=0;
  		while(count < bomb){
	    	
	    	int xBomb , yBomb;

			xBomb=(int)(Math.random()*squares); 
  			yBomb=(int)(Math.random()*squares); 


  			if( bombs[xBomb][yBomb] )
  				continue;

  			bombs[xBomb][yBomb] = true; 
  			count++;

	  	}
  	}

  	public void initializatedBombs(int squares){
  		
	    bombs = new Boolean[squares][squares];

  		for(int x=0; x<squares; x++){
  			for (int y =0; y<squares; y++){

  				bombs[x][y]=false;
  			}
  		}
  	}

	private void clicked(ActionEvent e, int[] position){
		

		//System.out.println(prop.isWinner);
		if(isSquearSelected(e) || !prop.isWinner   )
			return ;

		if( bombs[ position[0]][ position[1] ] )
			lost(e,position);
		else
			countinue(e,position);
	    
	}

	private Boolean isSquearSelected(ActionEvent e)
	{ 
		Button btn = (Button) e.getSource();
		return (!btn.getText().equals("*"));
	}

	private void lost(ActionEvent e, int[] position)
	{

		Button btnClicked = (Button	) e.getSource();
		btnClicked.setText("X");
		fillBoardLost();
		prop.isWinner=false;

	}

	private void fillBoardLost(){

		for(int y =0 ;  y < prop.squares.length ; y++ ){
	    	for(int x =0 ;  x < prop.squares.length ; x++ ){

	    		if(prop.squares[x][y].getText().equals("*") 
	    			&& bombs[x][y]   )
	    			prop.squares[x][y].setText("x");
	    	}
	    }
	}


	private void countinue(ActionEvent e, int[] position){

		int countBomb=0;
		int x = position[0];
		int y = position[1];
		
		updateStatus();


		Boolean haveLefSup 	=  haveBomb(x-1,y-1) ;
		Boolean haveLef 	=  haveBomb(x-1,y) ;
		Boolean haveInfLef 	=  haveBomb(x-1,y+1) ;

		Boolean haveSup =  haveBomb(x,y+1) ;
		Boolean haveInf =  haveBomb(x ,y-1) ;

		Boolean haveSupR =  haveBomb(x+1,y-1) ;
		Boolean haveR 	 =  haveBomb(x+1,y) ;
		Boolean haveInfR =  haveBomb(x+1,y+1) ;

		if(haveLefSup )
			countBomb ++;
		if(haveLef )
			countBomb ++;
		if(haveInfLef )
			countBomb ++;

		if(haveSup )
			countBomb ++;
		if(haveInf )
			countBomb ++;

		if(haveSupR )
			countBomb ++;
		if(haveR )
			countBomb ++;
		if(haveInfR )
			countBomb ++;

		Button btnClicked = (Button	) e.getSource();
		btnClicked.setText(""+countBomb);

	}

	private void updateStatus(){

		statusSquares --;
		statusRest ++;

  		String content = "Restantes: " +statusSquares +"\nSeleccionadas :"+statusRest;
		prop.lblSquares.setText(content);

		
	}

	private Boolean haveBomb(int x, int y){

		try{	

			return bombs [x][y];

		}catch(ArrayIndexOutOfBoundsException e){
	
			return false;

		}

	}


	public  void selectFile(ActionEvent e){
	
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		fileChooser.getExtensionFilters().addAll(
		         new ExtensionFilter("Text Files", "*.txt"));
		 File selectedFile = fileChooser.showOpenDialog(prop.stgContainter);
		 if (selectedFile != null) {
		 	try{
		 		analizeTxt(selectedFile);
		 	}catch(Exception a){}
		}
	}

	private void  analizeTxt(File file) throws IOException,FileNotFoundException{
		
		String content = new Scanner(file).useDelimiter("\\Z").next();
		//System.out.println(lenghtBoardLine(content));

		//loadGame(int squares, int bomb){
	}

	private int lenghtBoardLine (String board){
		
		return board.indexOf('\n');
	}


	public void loadGame( String bomb){
/*
  		loadBombs(bomb,squares);

  		setStatus(squares,bomb);
	    prop.panelStatus = createPanelStatus();

	    prop.panelGame = createBoard(squares);

	    prop.brdPContainter = new BorderPane();
	    prop.brdPContainter.setCenter(prop.panelGame);
	    prop.brdPContainter.setTop(prop.panelStatus);

	    prop.scnGame = new Scene(prop.brdPContainter, 600,600);
	    prop.stgContainter.setScene(prop.scnGame);
	    prop.stgContainter.show();*/
  	}


}