import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.paint.*;
import javafx.scene.shape.*;
import javafx.scene.Group;
import javafx.scene.layout.GridPane;
import javafx.geometry.*;
import javafx.scene.layout.*;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

 
public class App extends Application {
    

    private AppProperties prop;

    public App(){
        prop = AppProperties.getInstance();
    }   


    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) {


        prop.stgContainter=primaryStage;

        prop.btnEasy   =new Button("Easy");
        prop.btnMedium =new Button("Medium");
        prop.btnHard   =new Button("Hard");
        prop.btnFile   =new Button("Select file");

        prop.btnEasy.setOnAction         (e-> HandlerGame.getInstance().selectEasy(e));
        prop.btnMedium.setOnAction       (e-> HandlerGame.getInstance().selectMedium(e));
        prop.btnHard.setOnAction         (e-> HandlerGame.getInstance().selectHard(e));
        prop.btnFile.setOnAction         (e-> HandlerGame.getInstance().selectFile(e));

        prop.lblSelectMode =new Label("Select your difficulty: ");
        prop.lblFile = new Label("   o ");

        prop.panel =new FlowPane();
        prop.panel.setVgap(10);
        //prop.panel.setStyle("-fx-background-color: red;-fx-padding: 10px;");

        prop.panel.getChildren().addAll(prop.lblSelectMode,  prop.btnEasy, prop.btnMedium, prop.btnHard);
        prop.panel.getChildren().addAll(prop.lblFile, prop.btnFile);
        prop.scnMenu =new Scene(prop.panel, 600, 600);


        prop.stgContainter.setTitle("Busca minas");
        prop.stgContainter.setScene(prop.scnMenu);
        prop.stgContainter.show();
    }

   

}